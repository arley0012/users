import { Component, OnInit } from '@angular/core';
import * as firebase from 'firebase';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {

  ngOnInit() {
    firebase.initializeApp({
      apiKey: 'AIzaSyDVB16Qrk5YWmTwqI_4ZpQSP469-jUUy9c',
      authDomain: 'users-9941a.firebaseapp.com'
    });
  }
}
